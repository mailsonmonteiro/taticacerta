require 'test_helper'

class CartoesControllerTest < ActionController::TestCase
  setup do
    @cartao = cartoes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cartoes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cartao" do
    assert_difference('Cartao.count') do
      post :create, cartao: {  }
    end

    assert_redirected_to cartao_path(assigns(:cartao))
  end

  test "should show cartao" do
    get :show, id: @cartao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cartao
    assert_response :success
  end

  test "should update cartao" do
    patch :update, id: @cartao, cartao: {  }
    assert_redirected_to cartao_path(assigns(:cartao))
  end

  test "should destroy cartao" do
    assert_difference('Cartao.count', -1) do
      delete :destroy, id: @cartao
    end

    assert_redirected_to cartoes_path
  end
end
