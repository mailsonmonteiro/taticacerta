require 'test_helper'

class RespostasFsControllerTest < ActionController::TestCase
  setup do
    @resposta_fs = respostas_fs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:respostas_fs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create resposta_fs" do
    assert_difference('RespostaFs.count') do
      post :create, resposta_fs: {  }
    end

    assert_redirected_to resposta_fs_path(assigns(:resposta_fs))
  end

  test "should show resposta_fs" do
    get :show, id: @resposta_fs
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @resposta_fs
    assert_response :success
  end

  test "should update resposta_fs" do
    patch :update, id: @resposta_fs, resposta_fs: {  }
    assert_redirected_to resposta_fs_path(assigns(:resposta_fs))
  end

  test "should destroy resposta_fs" do
    assert_difference('RespostaFs.count', -1) do
      delete :destroy, id: @resposta_fs
    end

    assert_redirected_to respostas_fs_path
  end
end
