require 'test_helper'

class TaticasControllerTest < ActionController::TestCase
  setup do
    @tatica = taticas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:taticas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tatica" do
    assert_difference('Tatica.count') do
      post :create, tatica: { link: @tatica.link, texto: @tatica.texto, titulo: @tatica.titulo }
    end

    assert_redirected_to tatica_path(assigns(:tatica))
  end

  test "should show tatica" do
    get :show, id: @tatica
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tatica
    assert_response :success
  end

  test "should update tatica" do
    patch :update, id: @tatica, tatica: { link: @tatica.link, texto: @tatica.texto, titulo: @tatica.titulo }
    assert_redirected_to tatica_path(assigns(:tatica))
  end

  test "should destroy tatica" do
    assert_difference('Tatica.count', -1) do
      delete :destroy, id: @tatica
    end

    assert_redirected_to taticas_path
  end
end
