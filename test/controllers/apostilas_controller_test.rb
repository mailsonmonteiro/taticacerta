require 'test_helper'

class ApostilasControllerTest < ActionController::TestCase
  setup do
    @apostila = apostilas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:apostilas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create apostila" do
    assert_difference('Apostila.count') do
      post :create, apostila: {  }
    end

    assert_redirected_to apostila_path(assigns(:apostila))
  end

  test "should show apostila" do
    get :show, id: @apostila
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @apostila
    assert_response :success
  end

  test "should update apostila" do
    patch :update, id: @apostila, apostila: {  }
    assert_redirected_to apostila_path(assigns(:apostila))
  end

  test "should destroy apostila" do
    assert_difference('Apostila.count', -1) do
      delete :destroy, id: @apostila
    end

    assert_redirected_to apostilas_path
  end
end
