# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

materias = Materia.create([
                            {nome: "fisica", display: "Física", cor: "blue", icone: "thermometer half"},
                            {nome: "quimica", display: "Química", cor: "blue", icone: "flask"},
                            {nome: "matematica", display: "Matemática", cor: "blue", icone: "calculator"},
                            {nome: "biologia", display: "Biologia", cor: "green", icone: "leaf"},
                            {nome: "historia", display: "História", cor: "red", icone: "hourglass half"},
                            {nome: "geografia", display: "Geografia", cor: "green", icone: "globe"},
                            {nome: "sociologia", display: "Sociologia", cor: "red", icone: "users"},
                            {nome: "filosofia", display: "Filosofia", cor: "red", icone: "heart"},
                            {nome: "portugues", display: "Língua Portuguesa", cor: "red", icone: "commenting"},
                            {nome: "literatura", display: "Literatura", cor: "red", icone: "book"},
                            {nome: "ingles", display: "Inglês", cor: "teal", icone: "comments"},
                            {nome: "espanhol", display: "Espanhol", cor: "teal", icone: "comments"},
                            {nome: "artes", display: "Artes", cor: "teal", icone: "paint brush"},
                            {nome: "todas", display: "Todas as Matérias", cor: "violet", icone: "paint brush"}
                            ])

#apostilas = Apostila.create([
#                            {nome: "fisica"},
#                            {nome: "quimica"},
#                            {nome: "matematica"},
#                            {nome: "biologia"},
#                            {nome: "historia"},
#                            {nome: "geografia"},
#                            {nome: "sociologia"},
#                            {nome: "filosofia"},
#                            {nome: "portugues"},
#                            {nome: "literatura"},
#                            {nome: "ingles"},
#                            {nome: "espanhol"},
#                            {nome: "artes"},
#                            {nome: "todas"}
#                            ])