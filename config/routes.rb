Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  root 'landing#main'
  
  #--------------------------------------
  #Páginas abertas
  #--------------------------------------
  #Sidebar
  get 'plano_de_estudo' => 'landing#painel'
  #get 'analises' => 'dashboard#dashboard'
  #get 'banco_de_questoes' => 'landing#banco_de_questoes'
  
  
  #Dashboard
  #get 'dashboard/bloco' => 'dashboard#bloco' #Ajax
  #get 'dashboard/assunto' => 'dashboard#assunto' #Ajax
  #get 'dashboard/exercicio' => 'dashboard#exercicio' #Ajax
  
  #Acesso às apostilas
  get 'provas_e_simulados' => 'apostilas#apostilas'
  get '/provas_e_simulados/:apostila/', to: 'apostilas#apostila'
  get '/provas_e_simulados/:apostila/:indice', to: 'apostilas#pagina'
  get '/corrige_cartao/:apostila', to: 'apostilas#corrige_cartao'
  
  #Login via omniauth
  get '/auth/:provider/callback', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get 'login' => 'landing#login'
  
  #Tópicos e assuntos
  get '/topicos/adiciona_apostila/:apostila/:topico', to: 'topicos#adiciona_apostila'
  get '/topicos/remove_apostila/:apostila/:topico', to: 'topicos#remove_apostila'
  get '/material_de_estudo/', to: 'assuntos#assuntos'
  get '/material_de_estudo/:assunto', to: 'assuntos#assunto'
  get '/material_de_estudo/flashcards/:nome_do_deck', to: 'decks#show_por_titulo'
  #get '/material_de_estudo/:assunto/:topico', to: 'assuntos#topico'
  
  #Flashcards
  get '/flashcard/img/:id', to: 'flashcards#img'

  #Retornar comentarios
  #get '/comments/questao/:questao_id', to: 'comentarios#por_questao' #AJAX
  
  get 'landing/main'
  
  #Teste Pagseguro
  get '/blog', to: 'blog#inicio'
  #get '/inicia_pagamento', to: 'blog#inicia_pagamento'
  #post '/notificacoes/', to: 'blog#notificacao'
  #get '/compra_concluida/', to: 'blog#compra_concluida'
  get '/marktex_editor/', to: 'blog#marktex_editor'
  
  #--------------------------------------
  #Páginas fechadas
  #--------------------------------------
  #AJAX todas as questões de uma matéria
  #get 'questoes_por_materia/:materia', to: 'landing#questoes_por_materia'
  #get 'nao_respondidas/:materia', to: 'landing#nao_respondidas'
  #get 'certas/:materia', to: 'landing#certas'
  #get 'erradas/:materia', to: 'landing#erradas'

  #Edição de apostilas
  get 'adiciona_questao/:questao/:apostila', to: 'apostilas#adiciona_questao' #AJAX
  get 'remove_questao/:questao/:apostila', to: 'apostilas#remove_questao' #AJAX

  #Edição de questões
  get 'assuntos/adiciona/:assunto/:questao', to: 'questoes#add_assunto'
  get 'assuntos/remove/:assunto/:questao', to: 'questoes#rem_assunto'
  get 'questao/define_gabarito/:id/:gabarito', to: 'questoes#define_gabarito' #AJAX
  get 'questao/exibir_formulario/:id', to: 'questoes#modal_classificacao' #AJAX
  get '/importar_questoes', to: 'questoes#importar'
  
  #Seleção de cursos
  get 'sessions/novo-curso/:novo_curso', to: 'sessions#seleciona_curso'
  
  resources :respostas
  resources :questoes
  
  scope '/admin' do
    resources :assuntos
    resources :taticas
    resources :cartoes
    resources :flashcards
    resources :comentarios
    resources :cursos
    resources :topicos
    resources :apostilas
    resources :decks
    resources :concursos
    resources :respostas_fs
    resources :semanas
    resources :estudos
  end

  #Importar topicos do contentful
  #get 'importa_topicos', to: 'topicos#importar'
  #get 'importa_assuntos', to: 'assuntos#importar'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
