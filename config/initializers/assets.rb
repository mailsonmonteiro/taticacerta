# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( provas_e_simulados.js )
Rails.application.config.assets.precompile += %w( dashboard.css )
Rails.application.config.assets.precompile += %w( assuntos.css )
Rails.application.config.assets.precompile += %w( login.css )
Rails.application.config.assets.precompile += %w( main.css )
Rails.application.config.assets.precompile += %w( main.js )
Rails.application.config.assets.precompile += %w( painel.css )
Rails.application.config.assets.precompile += %w( sidebar.css )
Rails.application.config.assets.precompile += %w( layout.css )
Rails.application.config.assets.precompile += %w( questoes.css )
Rails.application.config.assets.precompile += %w( blog.css )
Rails.application.config.assets.precompile += %w( lista.css )
Rails.application.config.assets.precompile += %w( ckeditor/*)
Rails.application.config.assets.precompile += %w(ckeditor/config.js)
Rails.application.config.assets.precompile += %w( flashcards.css )
Rails.application.config.assets.precompile += %w( flashcards.js )
Rails.application.config.assets.precompile += %w( apostilas.css )
#Redação
Rails.application.config.assets.precompile += %w( redacao.css )
Rails.application.config.assets.precompile += %w( redacao.js )