class Grafico
  def Grafico.numero_de_acertos
    @match = {
      "$match": {
      "user_id": 'ObjectId("5adcab036ffd570d993b0c2d")'
      }
    }
    
    @group = {
      "$group": {
        _id: { "$dateToString": { "format": "%d/%m/%Y", "date": "$created_at" } }, 
        count: { "$sum": 1.0 }
      }
    }
    
    @sort = { '$sort': { _id: 1.0 } }
    
    resp = []
    
    (0..5).each do |i|
      data = Date.today - (5-i)
      resp << [data.strftime("%d/%m"), Resposta.where(:created_at.lte => data + 1, :created_at.gt => data).count]
    end
  
    resp
  end
end