json.array!(@comentarios) do |comentario|
  json.extract! comentario, :id, :user_id, :texto
  json.url comentario_url(comentario, format: :json)
end
