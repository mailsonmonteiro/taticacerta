json.array!(@semanas) do |semana|
  json.extract! semana, :id, :titulo, :lancamento
  json.url semana_url(semana, format: :json)
end
