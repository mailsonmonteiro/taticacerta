json.array!(@estudos) do |estudo|
  json.extract! estudo, :id, :inicio, :fim, :assuntos, :pagina_inicio, :pagina_fim
  json.url estudo_url(estudo, format: :json)
end
