json.array!(@respostas) do |resposta|
  json.extract! resposta, :id, :user, :questao, :alternativa
  json.url resposta_url(resposta, format: :json)
end
