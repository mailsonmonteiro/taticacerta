json.array!(@concursos) do |concurso|
  json.extract! concurso, :id, :titulo, :display
  json.url concurso_url(concurso, format: :json)
end
