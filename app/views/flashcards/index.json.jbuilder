json.array!(@flashcards) do |flashcard|
  json.extract! flashcard, :id, :titulo, :display, :texto
  json.url flashcard_url(flashcard, format: :json)
end
