json.array!(@cartoes) do |cartao|
  json.extract! cartao, :id
  json.url cartao_url(cartao, format: :json)
end
