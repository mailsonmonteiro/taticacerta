json.array!(@taticas) do |tatica|
  json.extract! tatica, :id, :link, :titulo, :texto
  json.url tatica_url(tatica, format: :json)
end
