json.array!(@apostilas) do |apostila|
  json.extract! apostila, :id
  json.url apostila_url(apostila, format: :json)
end
