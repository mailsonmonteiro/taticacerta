json.array!(@cursos) do |curso|
  json.extract! curso, :id, :nome, :display, :descricao, :concurso
  json.url curso_url(curso, format: :json)
end
