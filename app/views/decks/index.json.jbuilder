json.array!(@decks) do |deck|
  json.extract! deck, :id, :titulo, :user_id, :descricao
  json.url deck_url(deck, format: :json)
end
