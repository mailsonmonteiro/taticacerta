json.array!(@topicos) do |topico|
  json.extract! topico, :id
  json.url topico_url(topico, format: :json)
end
