json.array!(@respostas_fs) do |resposta_fs|
  json.extract! resposta_fs, :id
  json.url resposta_fs_url(resposta_fs, format: :json)
end
