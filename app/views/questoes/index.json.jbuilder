json.array!(@questoes) do |questao|
  json.extract! questao, :id
  json.url questao_url(questao, format: :json)
end
