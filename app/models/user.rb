class User
  include Mongoid::Document
  field :provider, type: String
  field :uid, type: String
  field :name, type: String
  field :location, type: String
  field :image_url, type: String
  field :url, type: String
  field :email, type: String
  field :curso_atual, type: String
  field :assinaturas, type: Array, default: []
  field :status, type: String
  field :role
  field :admin, type: Boolean
  
  has_many :respostas
  
  scope :eu, -> { where(name: "Mailson Monteiro") }
  
  class << self
    def from_omniauth(auth_hash)
      user = find_or_create_by(uid: auth_hash['uid'], provider: auth_hash['provider'])
      user.name = auth_hash['info']['name']
      user.location = get_social_location_for user.provider, auth_hash['info']['location']
      user.image_url = auth_hash['info']['image']
      user.url = get_social_url_for user.provider, auth_hash['info']['urls']
      user.email = auth_hash['info']['email']
      user.curso_atual = Curso.first.id.to_s
      user.save!
      user
    end

    private

    def get_social_location_for(provider, location_hash)
      case provider
        when 'linkedin'
          location_hash['name']
        else
          location_hash
      end
    end

    def get_social_url_for(provider, urls_hash)
      case provider
        when 'linkedin'
          urls_hash['public_profile']
        else
          urls_hash[provider.capitalize]
      end
    end
  end
  
  #FUNCÕES ESPECÍFICAS DO SITE
  def desempenho_no_topico(topico)
    acertos = self.respostas.where(topicos: topico.id.to_s).where(certa: true).distinct(:questao_id).count
    erros = self.respostas.where(topicos: topico.id.to_s).where(certa: false).distinct(:questao_id).count
    desempenho = if acertos+erros > 0 then (acertos/(acertos+erros.to_f)*100).round(0) else 0 end

    desempenho
  end

  def desempenho_por_cartao(cartao_id)
    desempenho = {
      erros: self.respostas.where(cartao_id: cartao_id, alternativa: 1).length,
      quases: self.respostas.where(cartao_id: cartao_id, alternativa: 2).length,
      acertos: self.respostas.where(cartao_id: cartao_id, alternativa: 3).length
    }
    desempenho
  end
  
end
