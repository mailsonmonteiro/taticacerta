class Assunto
  include Mongoid::Document
  field :nome, type: String
  field :display, type: String
  field :descricao, type: String
  field :indice, type: Integer
  
  belongs_to :materia
  has_many :topicos
  has_many :taticas
  has_many :decks
  
  default_scope { order_by(:indice => 'asc') }
  
  
  def to_label
    "#{indice} - #{display}"
  end
  
  def questoes
    Questao.where(assuntos: self.id)
  end

#Agrupa Táticas e decks de um mesmo assunto
  def itens
    itens = AgrupaItens.new
    itens.cadastrar self.taticas
    itens.cadastrar self.decks

    itens.itens
  end

end
