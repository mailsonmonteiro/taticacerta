class Plano
  include Mongoid::Document
  field :codigo
  field :nome
  field :mensalidade
  field :frequencia
  field :data_final
  
  belongs_to :user
  has_many :assinaturas
end
