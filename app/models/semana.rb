class Semana
  include Mongoid::Document
  field :titulo, type: String
  field :inicio

  has_many :topicos
  has_many :taticas
  has_many :apostilas

  def to_label
  	"#{self.titulo}"
  end
end
