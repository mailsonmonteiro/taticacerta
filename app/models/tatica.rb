class Tatica
  include Mongoid::Document
  include Mongoid::Timestamps
  field :link, type: String
  field :titulo, type: String
  field :texto, type: String
  field :topicos, type: Array, default: []
  
  belongs_to :user
  belongs_to :questao
  belongs_to :semana
  belongs_to :assunto

  def topicos_obj
    Topico.where(:id.in => self.topicos)
  end

  def to_item
    item = {
      titulo: self.titulo,
      descricao: '',
      link: self.link,
      tipo: self.class.to_s,
      icone: 'file alternate outline',
      obj: self,
      criado_em: self.created_at,
      atualizado_em: self.updated_at 
    }
  end
end
