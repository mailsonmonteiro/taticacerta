class Questao
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :banca, type: String
  field :ano, type: String
  field :orgao, type: String
  field :prova, type: String
  field :gabarito, type: String
  field :alternativa1, type: String
  field :alternativa2, type: String
  field :alternativa3, type: String
  field :alternativa4, type: String
  field :alternativa5, type: String
  field :texto_auxiliar, type: String
  field :identificador, type: String
  field :assuntos, type: Array, default: []
  field :topicos, type: Array, default: []
  field :resolucao, type: String
  
  validates :alternativa1, length: { minimum: 1 }
  validates :alternativa2, length: { minimum: 1 }
  
  validates_presence_of :materia_id, message: "A questão deve pertencer a uma matéria"
  
  belongs_to :materia
  has_many :resposta
  has_many :comentarios
  has_many :taticas
  
  scope :do_curso, ->(curso) { where(:materia_id.in => curso.materias) }
  scope :por_materia, ->(materias) { where(:materia_id.in => materias) }
  scope :por_assunto, ->(assuntos) { where(:assuntos.in => assuntos) }
  scope :respondidas, ->(user) { where(:id.in => user.questoes_respondidas) }
  scope :nao_respondidas, ->(user) { where(:id.nin => user.questoes_respondidas) }
  
  scope :filtra_materia, ->(materia_filtro){ 
    if materia_filtro.nome == "todas"
      where(:materia.nin => ["", nil])
    else
      where(materia: materia_filtro) 
    end
  }
  
  def alertas
    alerta = []
    if self.topicos.size <= 1 then alerta << "A questão não possúi tópicos" end
    if self.topicos.size <= 1 then alerta << "A questão não possúi assunto" end
    if self.gabarito == "" then alerta << "A questão não possúi gabarito" end
    alerta
  end
  
  def respondida(user)
    if user.respostas.where(questao: self._id).size > 0
      true
    else
      false
    end
  end
  
  def respostas_certas(user)
    respostas = user.respostas.where(questao: self._id)
    
    cont = 0
    respostas.each do |r|
      if r.certa then cont += 1 end
    end
    cont
  end
  
  def respostas_erradas(user)
    respostas = user.respostas.where(questao: self._id)
    
    cont = 0
    respostas.each do |r|
      if not r.certa then cont += 1 end
    end
    cont
  end
  
  def indice(apostila)
    a = Apostila.find_by(nome: apostila)
    i = 0
    
    if a != nil
      i = a.questoes.index(self.id)
      logger.debug "###################" 
      logger.debug i
      i += 1
    else
      i
    end
  end
  
  def link(apostila)
    l = ""
    if self.materia != nil
      l ="/apostila/#{apostila}/#{self.indice(apostila)}"
    end
    l
  end
  
end
