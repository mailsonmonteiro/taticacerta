class Notification
  include Mongoid::Document
  field :code, type: String
  field :type, type: String
  
  has_one :checkout
end
