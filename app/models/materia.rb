class Materia
  include Mongoid::Document
  field :nome, type: String
  field :display, type: String
  field :cor, type: String
  field :icone, type: String
  field :abrev, type: String
  
  has_many :assuntos
  #has_many :
  
  #Necessário para o siple_forms apresentar o display name da questão em vez do id no formulário
  def to_label
    "#{display}"
  end
  
  scope :bio, ->{ where(nome: "biologia") }
  scope :exatas, ->{ where(:nome.in => [ "matematica","fisica" ]) }
  scope :matematica, ->{ where(nome: "matematica") }
  scope :do_curso, -> (curso_atual) { where(:id.in => curso_atual.materias.to_a) }
  
end