class Resposta
  include Mongoid::Document
  include Mongoid::Timestamps
  field :alternativa, type: String
  field :lista, type: String
  field :curso, type: String
  field :materia, type: String
  field :topicos, type: Array, default: []
  field :assuntos, type: Array, default: []
  field :certa, type: Boolean, default: false
  field :gabarito, type: String
  
  belongs_to :questao
  belongs_to :user
  belongs_to :apostila
  belongs_to :cartao
  belongs_to :flashcard
  belongs_to :deck
  
  scope :por_apostila, -> (apostila) { where(:questao.in => apostila.questoes) }
  scope :por_assunto, -> (assunto) { where(assuntos: assunto.id.to_s) }
  
  before_save :verifica_gabarito
  before_update :verifica_gabarito
  
  def verifica_gabarito
    if questao!= nil then self.gabarito = self.questao.gabarito end #Respota a questões padrão
    if flashcard!= nil then self.gabarito = 3 end #Resposta a flashcards

    self.certa = self.alternativa == self.gabarito

    logger.debug "------------"
    logger.debug self.alternativa
    logger.debug self.gabarito
    logger.debug self.certa.to_s
  end

  def como_icones(gabarito = false)
    icones = []

    i = 0
    ["a","b","c","d","e"].each do |a|
      g = (gabarito && self.gabarito == a)? 'blue' : '' #cor azul para o gabarito
      o = self.alternativa == a ? '' : 'outline' #sem outline para a resposta marcada
      
      icones[i] = "<i class='#{g} circle #{o} icon'></i>"

      icones[i]
      i += 1
    end
    icones
  end

end
