class Estatistica
  include Mongoid::Document
  include Mongoid::Timestamps
  field :chave
  field :valor
  
  belongs_to :user
end
