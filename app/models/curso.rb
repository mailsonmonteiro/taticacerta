class Curso
  include Mongoid::Document
  field :nome, type: String
  field :display, type: String
  field :descricao, type: String
  field :concurso, type: String
  field :materias, type: Array, default: []
  
  def to_label
    "#{display}"
  end
  
end
