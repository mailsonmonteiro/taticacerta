class Apostila
  include Mongoid::Document
  
  field :nome, type: String
  field :descricao, type: String #Descrição curta
  field :texto, type: String #Descrição longa
  field :questoes, type: Array, default: []
  field :materias, type: Array, default: []
  field :assuntos, type: Array, default: []
  field :topicos, type: Array, default: []
  field :imagem, type: String
  field :display, type: String
  
  belongs_to :curso
  belongs_to :semana
  has_many :respostas
  has_many :cartoes
  
  validates_presence_of :nome, :descricao, :display, :materias
  
  before_save :adiciona_assuntos_e_topicos
  
  scope :por_materia, -> (materia) { where :materias.in => materia }
  scope :respondidas, -> (user) { where :id.in => user.apostilas_respondidas }
  scope :nao_respondidas, -> (user) { where :id.nin => user.apostilas_respondidas }
  scope :do_curso, -> (curso_atual) { Apostila.where(curso: curso_atual) }
  
  def adiciona_assuntos_e_topicos
    self.assuntos.clear
    self.topicos.clear
    assuntos = Set.new
    topicos = Set.new
    
    self.questoes.each do |qq|
      q = Questao.where(id: qq)
      next if q.size == 0
      q = q.first
      
      q.assuntos.each do |a|
        assuntos.add(a.to_s)
      end
      q.topicos.each do |t|
        topicos.add(t.to_s)
      end
    end
    
    self.assuntos = assuntos.to_a
    self.topicos = topicos.to_a
  end

  def topicos_obj
    Topico.where(:id.in => self.topicos)
  end
  
  def to_label
    self.display
  end
  
  def link
    "/provas_e_simulados/#{self.nome}"
  end
  
  def respondida(user)
    user.respostas.por_apostila(self).size > 0
  end
  
  #def respostas(user)
  #  user.respostas.por_apostila(self)
  #end
  
  def respostas_certas(user)
    questoes = self.questoes
    
    total = 0
    
    questoes.each do |qq|
      q = Questao.find_by(id: qq)
      total += q.respostas_certas(user)
    end
    total
  end
  
  def percentual(user)
    per = "#{respostas_certas(user)} / #{self.questoes.size}"
    per
  end
  
  def gabaritada(user)
    respostas_certas(user) == self.questoes.size
  end
end
