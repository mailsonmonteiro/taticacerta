class Flashcard
  include Mongoid::Document
  include Mongoid::Timestamps
  field :pergunta, type: String, default: ""
  field :resposta, type: String, default: ""
  
  belongs_to :deck
end
