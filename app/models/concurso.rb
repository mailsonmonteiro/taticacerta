class Concurso
  include Mongoid::Document
  field :titulo, type: String
  field :display, type: String
end
