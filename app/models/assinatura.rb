class Assinatura
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :code
  field :date
  field :tracker
  field :status
  field :reference
  
  belongs_to :user
  belongs_to :plano
end
