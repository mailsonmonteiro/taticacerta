class Comentario
  include Mongoid::Document
  include Mongoid::Timestamps
  field :texto, type: String
  
  default_scope { order_by(:created_at => 'asc') }
  
  belongs_to :user
  belongs_to :topico
  belongs_to :questao
  
  def dono
    if self.questao != nil
      self.questao
    elsif self.topico != nil
      self.topico
    end
  end
end
