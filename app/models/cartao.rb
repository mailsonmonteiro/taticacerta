class Cartao
  include Mongoid::Document
  include Mongoid::Timestamps
  field :aberto, type: Boolean, default: true
  field :questoes, type: Array, default: []
  field :finalizado_em
  
  has_many :respostas
  belongs_to :apostila
  belongs_to :user
  belongs_to :deck
  
  before_save :atualiza_questoes

  def atualiza_questoes
    return unless self.apostila != nil
    self.questoes = []
    
    self.apostila.questoes.each do |q|
      self.questoes << q
    end
  end

  def desempenho
    Desempenho.do_usuario_no_deck self
  end
end
