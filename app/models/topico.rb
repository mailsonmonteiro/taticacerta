class Topico
  include Mongoid::Document
  field :nome, type: String
  field :titulo, type: String
  field :texto, type: String
  field :indice, type: Float
  field :exemplo, type: String
  field :testes, type: Array, default: []
  
  belongs_to :assunto
  belongs_to :semana
  has_many :comentarios
  has_many :flashcards
  
  default_scope { order_by(:indice => 'asc') }
  
  def to_label
    "#{self.titulo}"
  end
  
  def link
    "/material_de_estudo/#{self.assunto.nome}/#{self.nome}"
  end
end
