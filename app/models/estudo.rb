class Estudo
  include Mongoid::Document
  field :inicio
  field :fim
  field :assuntos, type: String
  field :pagina_inicio, type: Integer
  field :pagina_fim, type: Integer
end
