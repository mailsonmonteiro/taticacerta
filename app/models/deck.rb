class Deck
  include Mongoid::Document
  include Mongoid::Timestamps
  field :titulo, type: String
  field :link, type: String
  field :descricao, type: String
  field :topicos, type: Array, default: []

  belongs_to :user
  has_many :flashcards
  has_many :cartoes
  belongs_to :assunto

  before_save :set_link

  def set_link
    if self.link == nil || self.link == '' then self.link = CGI.escape(self.titulo) end
  end

  def to_item
	  item = {
	    titulo: self.titulo,
	    descricao: self.descricao,
	    link: 'flashcards/'+self.titulo,
      tipo: self.class.to_s,
      icone: 'images outline',
      obj: self,
	    criado_em: self.created_at,
	    atualizado_em: self.updated_at 
	  }
  end

  def topicos_obj
    Topico.where(:id.in => self.topicos)
  end

end
