class RespostaFs
  include Mongoid::Timestamps
  include Mongoid::Document
  field :desempenho

  belongs_to :user
  belongs_to :flashcard
  belongs_to :deck
end
