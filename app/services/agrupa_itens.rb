class AgrupaItens
	def initialize
		@itens = []
	end

	def itens()
			@itens.sort_by { |hsh| hsh[:criado_em] }
	end

	def cadastrar itens
		itens.each do |i|
			@itens << i.to_item
		end
	end
end