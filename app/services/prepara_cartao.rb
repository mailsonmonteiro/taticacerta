class PreparaCartao
	def self.resposta_por_flashcard (cartao) 
		respostas = {}
    Resposta.where(cartao_id: cartao.id).each do |r|
      respostas[r.flashcard_id] = r
    end
    
    #monta o hash resposta por flashcard
    resposta_por_flashcard = []
    nova_resposta = Resposta.new

    cartao.deck.flashcards.each do |f|
      resposta_por_flashcard.append({
        flashcard: f,
        resposta: respostas[f.id] != nil ? respostas[f.id] : nova_resposta
      })
    end 
    resposta_por_flashcard
	end
end
