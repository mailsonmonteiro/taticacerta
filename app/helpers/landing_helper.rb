module LandingHelper
  
  def card_sugestao (titulo, tabela)
    render partial: "card_sugestao", locals:{titulo: titulo, tabela: tabela}
  end
  
  def gera_estrelas(tx)
    estrelas = ''
    outline = ''
    (1..3).each do |i|
      tx >= (i-1) * 0.45 ? outline = '' : outline = 'outline'
      estrelas << "<i class='small star #{outline} icon'></i>"
    end
    estrelas
  end
end
