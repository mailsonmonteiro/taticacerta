module ApostilaHelper
  def apostila_gera_icones(mostra_gabarito = false, gabarito = '', alternativa = '')
    icones = []

    i = 0
    ["a","b","c","d","e"].each do |a|
      g = (mostra_gabarito && gabarito == a)? 'blue' : '' #cor azul para o gabarito
      o = alternativa == a ? '' : 'outline' #sem outline para a resposta marcada
      
      icones[i] = "<i class='#{g} circle #{o} icon'></i>"

      icones[i]
      i += 1
    end
    icones
  end

end
