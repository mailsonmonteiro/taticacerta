module ApplicationHelper
  def contentful
    @client ||= Contentful::Client.new(
      access_token: "196f7e214bc56b834401b191907a021fd2ce2ec377c0854e63078910843857dd",
      space: "h8r98awupeov",
      dynamic_entries: :auto,
      raise_errors: true
    )
  end
  
  def exibe_header(texto, icone, bc)
    render partial: "layouts/header", locals:{texto: texto, icone: icone, bc: bc} 
  end
end
