module EstudosHelper
  def tabela_estudos(estudos)
    render partial: "estudos/estudos", locals:{estudos: estudos}
  end
end
