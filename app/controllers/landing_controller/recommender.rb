class LandingController
  class Recommender
    
    def Recommender.ponto_forte
      resp = Resposta.collection.aggregate [@uw_ass, @uw_top, @group, @add_total, @filtra_total, @add_aprov, @desc]
      ponto_forte = {}
      ponto_forte['assunto'] = Assunto.find_or_initialize_by(id: resp.first['_id'])
      ponto_forte['taxa_de_acerto'] = resp.first['aproveitamento'].to_f * 100 
      ponto_forte['realizados'] = resp.first['total']
      
      ponto_forte
    end
    
    def Recommender.ponto_fraco
      resp = Resposta.collection.aggregate [@uw_ass, @uw_top, @group, @add_total, @add_aprov, @asc]
      ponto_fraco = {}
      ponto_fraco['assunto'] = Assunto.find_or_initialize_by(id: resp.first['_id'])
      ponto_fraco['taxa_de_acerto'] = resp.first['aproveitamento'].to_f * 100 
      ponto_fraco['realizados'] = resp.first['total']
      
      ponto_fraco
    end
    
    def Recommender.revisar
      resp = Resposta.collection.aggregate [@uw_ass, @uw_top, @group, @add_total, @add_aprov, @total_asc]
      revisar = {}
      revisar['assunto'] = Assunto.find_or_initialize_by(id: resp.first['_id'])
      revisar['taxa_de_acerto'] = resp.first['aproveitamento'].to_f * 100 
      revisar['realizados'] = resp.first['total']
      
      revisar
    end
    
    def Recommender.tabela
      tabela = Resposta.collection.aggregate [@uw_ass, @uw_top, @group, @add_total, @add_aprov, @desc]
      tabela
    end
    
    #Etapas disponíveis da pipeline
    
    @uw_ass = { '$unwind': { "path": "$assuntos" } }
    @uw_top = { '$unwind': { "path": "$topicos" } }
    @group = {
      '$group': {
        _id: '$assuntos', 
        acertos: { '$sum': { '$cond': [ '$certa', {'$sum': 1}, {'$sum': 0} ] } },
        erros: { '$sum': { '$cond': [ '$certa', {'$sum': 0}, {'$sum': 1} ] } }
      }
    }
    
    @add_total = {
      '$addFields': {
        total: {'$sum': ['$erros', '$acertos']}
      }
    }
    
    @filtra_total = { '$match': { 'total': {'$gt': 3.0} } }
    
    @add_aprov = {
      '$addFields': {
          aproveitamento: {'$divide': ['$acertos', '$total']}
      }
    }
    
    @desc = { '$sort': { aproveitamento: -1.0 } }
    @asc = { '$sort': { aproveitamento: 1.0 } }
    @total_asc = { '$sort': { total: 1.0 } }
  end
end