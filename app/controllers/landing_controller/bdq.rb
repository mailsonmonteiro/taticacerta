  def banco_de_questoes
    @pagina_atual = "banco"
    materias = curso_atual.materias.to_a
    @filtros_aplicados = []
    @materias = Materia.do_curso(curso_atual)
    @assuntos = Assunto.where(:materia.in => materias)
    @questoes = Questao.do_curso(curso_atual)
    @respostas = []
    @comentario = Comentario.new
    
    @questoes.each do |q|
      @respostas.push(Resposta.new)
    end
    
    #Filtrar por matéria
    filtro_materia = []
    if params[:materias].present?
      params[:materias].each do |m|
        materia = Materia.where(nome: m).first
        if materia != nil
          filtro_materia.push(materia.id)
        end
        @filtros_aplicados.push(m)
      end
      @questoes = @questoes.por_materia(filtro_materia)
    end
    
    #Filtrar por assunto
    filtro_assunto = []
    if params[:assuntos].present?
      params[:assuntos].each do |a|
        assunto = Assunto.where(nome: a).first
        if assunto != nil
          filtro_assunto.push(assunto.id)
        end
        @filtros_aplicados.push(a)
      end
      
      @questoes = @questoes.por_assunto(filtro_assunto)
    end
    
    #Filtrar por atividade
    
    if params[:atividade].present?
      atv = params[:atividade]
      @filtros_aplicados.push(atv)
      if atv == "respondidas"
        @questoes = @questoes.respondidas(current_user)
      else
        @questoes = @questoes.nao_respondidas(current_user)
      end
    end
    
  end