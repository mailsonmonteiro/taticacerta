class LandingController < ApplicationController
  #Filtra Usuário Logado
  before_action :authenticate_user, :except => [:main, :login]
  
  def authenticate_user
    if !current_user
      redirect_to "/login"
    end
  end
  
  def main
    render(layout: "layouts/landing_page")
  end
  
  def login
    if current_user
      redirect_to "/plano_de_estudo/"
    else
      render(layout: "layouts/landing_page")
    end
  end
  
  def painel
    @pagina_atual = "painel"
    @estudos = Estudo.all
  end
  
  def listas
    @pagina_atual = "listas"
    materias = curso_atual.materias.to_a
    @apostilas = Apostila.do_curso(curso_atual)
    @materias = Materia.do_curso(curso_atual)
    @assuntos = Assunto.where(:materia.in => materias)
    @filtros_aplicados = []
    
    #Filtrar por matéria
    filtro_materia = []
    if params[:materias].present?
      params[:materias].each do |m|
        materia = Materia.where(nome: m).first
        if materia != nil
          filtro_materia.push(materia.id.to_s)
        end
        @filtros_aplicados.push(m)
      end
      @apostilas = @apostilas.por_materia(filtro_materia)
    end
    
    #Filtrar por assunto
    filtro_assuntos = []
    if params[:assuntos].present?
      params[:assuntos].each do |a|
        assunto = Assunto.where(nome: a).first
        if assunto != nil
          filtro_assuntos.push(assunto.id.to_s)
        end
        @filtros_aplicados.push(a)
      end
      @apostilas = @apostilas.where(:assuntos.in => filtro_assuntos)
    end
    
    #Filtrar por respostas
    if params[:atividade].present?
      atv = params[:atividade]
      if atv == "respondidas"
        @apostilas = @apostilas.respondidas(current_user)
      elsif
        @apostilas = @apostilas.nao_respondidas(current_user)
      end
      @filtros_aplicados.push(atv)
    end
    
  end
  
  private
  def set_materia
    Materia.find_by(nome: params[:materia])
  end
  
  def check_de_integridade_da_apostila
    aps = Apostila.all
    
    aps.each do |a|
      a.questoes.each do |q|
        if Questao.where(id: q).size == 0 
          a.questoes.delete(q)
          a.save
        end
      end
    end
  end
  
end

