class QuestoesController < ApplicationController
  before_action :set_questao, only: [:show, :edit, :update, :destroy, :define_gabarito, :modal_classificacao]
  before_action :admin_only
  
  def admin_only
    if !current_user.admin
      redirect_to "/"
    end
  end
  
  #Importar questões
  def importar
    @questao = Questao.new
    @questao.materia = Materia.find('5a81a30c6ffd570ddf6b2cbc')
  end
  
  ###############
  # AJAX - Define remotamente o gabarito de uma questão
  ##############
  def define_gabarito
    gabarito = params[:gabarito]
    
    @questao.gabarito = gabarito
    @questao.save
  end
  
  ###############
  # AJAX - Formulário de classificacao
  ##############
  def modal_classificacao
    lista_materias
    lista_apostilas
    lista_assuntos
    lista_topicos
    render "exibe_modal"
  end

  # GET /questoes
  # GET /questoes.json
  def index
    @questoes = Questao.all.order_by(created_at: 'desc')
    lista_apostilas
  end

  # GET /questoes/1
  # GET /questoes/1.json
  def show
    @a = [["A)", "a"]]
    @b = ["B)", "b"]
    @c = ["C)", "c"]
    @d = ["D)", "d"]
    @e = nil
    if @questao.alternativa5 != "" then
      @e = ["E)", "e"]
    end
    
    
    #Inicia uma nova resposta e busca as respostas anteriores
    @resposta = Resposta.new
    @respostas_anteriores = Resposta.where(user: current_user, questao: @questao)
  end

  # GET /questoes/new
  def new
    lista_materias
    lista_apostilas
    lista_assuntos
    lista_topicos
    
    @questao = Questao.new
  end

  # GET /questoes/1/edit
  def edit
    lista_materias
    lista_apostilas
    lista_assuntos
    lista_topicos
  end

  # POST /questoes
  # POST /questoes.json
  def create
    lista_materias
    lista_apostilas
    @questao = Questao.new(questao_params)

    respond_to do |format|
      if @questao.save
        format.html { redirect_to "/questoes/", notice: 'Questao was successfully created.' }
        format.json { render :show, status: :created, location: @questao }
        format.js {render "questao_importada"}
      else
        format.js {render "questao_importada"}
        format.html { render :new }
        format.json { render json: @questao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questoes/1
  # PATCH/PUT /questoes/1.json
  def update
    respond_to do |format|
      if @questao.update(questao_params)
        format.html { redirect_to "/questoes", notice: 'Questao was successfully updated.' }
        format.json { render :show, status: :ok, location: @questao }
        format.js { render "questao_salva" }
      else
        format.html { redirect_to :edit }
        format.json { render json: @questao.errors, status: :unprocessable_entity }
        format.js { render "erro_ao_salvar" }
      end
    end
  end

  # DELETE /questoes/1
  # DELETE /questoes/1.json
  def destroy
    if @questao.materia != nil
      a = Apostila.find_by(nome: @questao.materia.nome)
      if a.questoes.include?(@questao.id)
        a.questoes.delete(@questao.id)
      end
    end
    @questao.destroy
    respond_to do |format|
      format.html { redirect_to questoes_url, notice: 'Questao was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_assunto
    @questao = Questao.find_by(id: params[:questao]) 
    @assunto = Assunto.find_by(id: params[:assunto])
    lista_materias
    lista_apostilas
    lista_assuntos
    
    if !@questao.assuntos.include?(@assunto.id)
      @questao.add_to_set(assuntos: @assunto.id)
    end
    redirect_to "/questoes/#{@questao.id}/edit#assuntos"
  end
  
  def rem_assunto
    @questao = Questao.find_by(id: params[:questao]) 
    @assunto = Assunto.find_by(id: params[:assunto])
    lista_materias
    lista_apostilas
    lista_assuntos
    
    if @questao.assuntos.include?(@assunto.id)
      @questao.pull(assuntos: @assunto.id)
    end
    redirect_to "/questoes/#{@questao.id}/edit#assuntos"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_questao
      @questao = Questao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def questao_params
      params.require(:questao).permit(:id, :banca, :ano, :orgao, :prova, :gabarito, :alternativa1, :alternativa2, :alternativa3, :alternativa4,
      :alternativa5, :materia, :texto_auxiliar, :identificador, :resolucao, topicos: [], assuntos: [])
    end
    
    # Lista de Matérias disponíveis
    def lista_materias
      @materias = Materia.all
    end
    
    def lista_apostilas
      @apostilas = Apostila.all
    end
    
    def lista_assuntos
      @assuntos = Assunto.all
    end
    
    def lista_topicos
      @topicos = []
      
      if @questao != nil
        @questao.topicos.each do |t|
          if t != ""
            topico = Topico.find_by(id: t)
            @topicos.push(topico)
          end
        end
      
        @questao.assuntos.each do |a|
          assunto = Assunto.where(id: a)
          if assunto.size == 0
            @questao.pull(assuntos: a)
          else
            assunto.first.topicos.each do |t|
              if !@topicos.include?(t)
                @topicos.push(t)
              end
            end
          end
        end
      end
      
    end
    
    #Setup Materiais
    def set_materia(n)
      Materia.where(nome: n).first
    end
end
