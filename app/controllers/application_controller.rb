class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  require 'kramdown'

  around_action :collect_metrics

  def collect_metrics
    start = Time.now
    yield
    duration = Time.now - start
    Rails.logger.info "#{controller_name}##{action_name}: #{duration}s"
  end
  
  private
  
  def curso_atual
    curso = Curso.where(id: current_user.curso_atual).first
    
    if curso != ""
      @curso_atual = curso
    else
      @curso_atual = Curso.where(nome:"basa2018").first
    end
  end
  
  helper_method :curso_atual

  def current_user
    id = session[:user_uid]
    if id != nil
      @current_user = User.find_by(uid: id.to_s)
    else
      @current_user = nil
    end
  end
  
  helper_method :current_user
end
