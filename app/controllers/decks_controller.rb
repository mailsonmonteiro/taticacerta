class DecksController < ApplicationController
  before_action :set_deck, only: [:show, :edit, :update, :destroy]
  before_action :set_assuntos, only: [:edit, :new]

  # GET /decks
  # GET /decks.json
  def index
    @decks = Deck.all
  end

  # GET /decks/1
  # GET /decks/1.json
  def show
    user = current_user
    #Para carregar o desempenho anterior, basta definir o cartão
    @cartao = Cartao.find_or_create_by(user_id: user.id, deck_id: @deck.id)
    #@cartao = Cartao.create! user_id: current_user.id, deck_id: @deck.id
    
    @resposta_por_flashcard = PreparaCartao.resposta_por_flashcard @cartao

    @desempenho = user.desempenho_por_cartao @cartao.id
  end

  # GET /decks/new
  def new
    @deck = Deck.new
  end

  # GET /decks/1/edit
  def edit
    @flashcard = Flashcard.new
    @flashcard.deck_id = @deck.id
  end

  # POST /decks
  # POST /decks.json
  def create
    @deck = Deck.new(deck_params)

    respond_to do |format|
      if @deck.save
        format.html { redirect_to @deck, notice: 'Deck was successfully created.' }
        format.json { render :show, status: :created, location: @deck }
      else
        format.html { render :new }
        format.json { render json: @deck.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /decks/1
  # PATCH/PUT /decks/1.json
  def update
    respond_to do |format|
      if @deck.update(deck_params)
        format.html { redirect_to @deck, notice: 'Deck was successfully updated.' }
        format.json { render :show, status: :ok, location: @deck }
      else
        format.html { render :edit }
        format.json { render json: @deck.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /decks/1
  # DELETE /decks/1.json
  def destroy
    @deck.destroy
    respond_to do |format|
      format.html { redirect_to decks_url, notice: 'Deck was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deck
      if params[:id].present?
        @deck = Deck.find(params[:id])
      elsif params[:nome_do_deck].present?
        @deck = Deck.find_by(titulo: params[:nome_do_deck])
      end
    end

    def set_assuntos
      @assuntos = Assunto.all
      @topicos = Topico.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deck_params
      params.require(:deck).permit(:titulo, :user_id, :assunto_id, :descricao, topicos: [])
    end
end
