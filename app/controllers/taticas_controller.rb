class TaticasController < ApplicationController
  before_action :set_tatica, only: [:show, :edit, :update, :destroy]
  before_action :set_semanas_e_assuntos, only: [:edit, :new]

  # GET /taticas
  # GET /taticas.json
  def index
    @taticas = Tatica.all
  end

  # GET /taticas/1
  # GET /taticas/1.json
  def show
  end

  # GET /taticas/new
  def new
    @tatica = Tatica.new
  end

  # GET /taticas/1/edit
  def edit
  end

  # POST /taticas
  # POST /taticas.json
  def create
    @tatica = Tatica.new(tatica_params)

    respond_to do |format|
      if @tatica.save
        format.html { redirect_to @tatica, notice: 'Tatica was successfully created.' }
        format.json { render :show, status: :created, location: @tatica }
      else
        format.html { render :new }
        format.json { render json: @tatica.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /taticas/1
  # PATCH/PUT /taticas/1.json
  def update
    respond_to do |format|
      if @tatica.update(tatica_params)
        format.html { redirect_to @tatica, notice: 'Tatica was successfully updated.' }
        format.json { render :show, status: :ok, location: @tatica }
      else
        format.html { render :edit }
        format.json { render json: @tatica.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /taticas/1
  # DELETE /taticas/1.json
  def destroy
    @tatica.destroy
    respond_to do |format|
      format.html { redirect_to taticas_url, notice: 'Tatica was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tatica
      @tatica = Tatica.find(params[:id])
    end

    def set_semanas_e_assuntos
      @semanas = Semana.all
      @assuntos = Assunto.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tatica_params
      params.require(:tatica).permit(:link, :titulo, :texto, :semana_id, :assunto_id)
    end
end
