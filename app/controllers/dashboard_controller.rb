class DashboardController < ApplicationController
  before_action :define_pagina
  
  def define_pagina
    @pagina_atual = "dashboard"
  end
  
  def dashboard
  end
  
  def bloco
      
  end
  
  def exercicio
      
  end
  
  def assunto
    #Popula a tabela de desempenho por matéria
    @desempenho_por_materia = Array.new
    
    i = 0
    Materia.do_curso(curso_atual).each do |mat|
      assuntos = []
      
      mat.assuntos.each do |ass|
        linha = Hash.new
        
        next if Assunto.where(id: ass).size == 0 
        a = Assunto.find(ass)
        
        respondidas = current_user.questoes_respondidas_por_assunto(a)
        certas = 0
        aprov = 0
        
        linha = {
          materia: a.display,
          respondidas: respondidas.size,
          aprov: aprov
        }        
        assuntos.push(linha)
      end
      
      @desempenho_por_materia[i] = [mat.display, assuntos]
      i = i+1
    end
  end
  
end
