class BlogController < ApplicationController
    skip_before_action :verify_authenticity_token
    
    def contentful
      @client ||= Contentful::Client.new(
        access_token: "196f7e214bc56b834401b191907a021fd2ce2ec377c0854e63078910843857dd",
        space: "h8r98awupeov",
        dynamic_entries: :auto,
        raise_errors: true
      )
    end
    
    def marktex_editor
      txt = params[:txt]
      
      @html = Kramdown::Document.new(txt).to_html
    end
    
    def inicio
      @endpoint = "/inicia_pagamento"
    end
    
    def inicia_pagamento
      plano = cria_plano
      
      if plano['preApprovalRequest']['code'] != ""
        code = plano['preApprovalRequest']['code']
        
        redirect_to "https://sandbox.pagseguro.uol.com.br/v2/pre-approvals/request.html?code=#{code}"
      end
    end
    
    def notificacao
      #!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      #Precisa modificar: O fluxo correto é salvar a assinatura no db e então atualizá-la
      #De acordo com as notificações do PagSeguro
      #!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      n = Notification.new
      
      #Recebe a notificação
      n.code = params[:notificationCode]
      n.type = params[:notificationType]
      n.save
      
      #Realiza consulta ao Pagseguro e recebe a assinatura relativa
      response = verifica_notification(params[:notificationCode])
      
      #Procura pela assinatura e pelo usuário que gerou a assinatura
      user = User.where(id: response['preApproval']['reference'] ).first
      
      #Executa a atualização
      if response['preApproval'] != "" && user != nil
        status = response['preApproval']['status']
        
        if status == "ACTIVE" then user.status = "Ativo" end
        if status == "PENDING" then user.status = "Aguardando Pagamento" end
        if status == "CANCELLED" then user.status = "Cancelado" end
        
        user.save
        logger.debug status
      end
    end
    
    def compra_concluida
      user = current_user
      
      @code = params[:code]
      #verifica se a assinatura é válida e se está aprovada
      status = verifica_assinatura(@code)['preApproval']['status']
      
      if ['PENDING', 'ACTIVE', 'CANCELLED', 'CANCELLED_BY_RECEIVER', 'CANCELLED_BY_SENDER', 'EXPIRED'].include?(status)
        user.status = status
      end
        
      #adiciona a assinatura ao usuário
      if !user.assinaturas.include?(@code) then user.assinaturas.push(@code) end
      user.save
    end
    
    private
    
    def verifica_assinatura(assinatura)
      url= "https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals/#{assinatura}?"\
          "email=mailson.a.m.monteiro@gmail.com&token=EEA588AB89664CD0AA63AECF2E709C75"
      resp = HTTParty.get(url).parsed_response
      resp
    end
    
    def verifica_notification(n)
      url = "https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals/notifications/#{n}?"\
          "email=mailson.a.m.monteiro@gmail.com&token=EEA588AB89664CD0AA63AECF2E709C75"
      resp = HTTParty.get(url).parsed_response
      resp
    end
    
    def cria_plano
      url= "https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals/request"
        body = {
        email: "mailson.a.m.monteiro@gmail.com",
        token: "EEA588AB89664CD0AA63AECF2E709C75",
        preApprovalCharge: "auto",
        preApprovalName: "Tatica Certa Sandbox",
        preApprovalAmountPerPayment: "25.00",
        preApprovalPeriod: "MONTHLY",
        preApprovalFinalDate: (Date.today+90).to_s(:db),
        preApprovalMaxTotalAmount: "75.00",
        reference: current_user.id 
      }
      
      resp = HTTParty.post(url,  
                        body: body,
                        headers: { 'Content-Type' => "application/x-www-form-urlencoded", 'charset' => "ISO-8859-1"}  )
      resp
    end
    
end
