class AssuntosController < ApplicationController
    
  def assunto
    @assunto = Assunto.find_by(nome: params[:assunto])
    @topicos = Topico.where(assunto: @assunto.id)
    @assuntos = Assunto.all
    @estatisticas = Hash.new
    @estatisticas['respostas'] = 0
    @estatisticas['certas'] = 0
    @estatisticas['aproveitamento'] = 0
    
    respondidas = current_user.questoes_respondidas
    
    respondidas.each do |q|
      if q.assuntos.include?(@assunto.id) 
        @estatisticas['respostas'] += 1 
        if q.respostas_certas(current_user) > 0 then @estatisticas['certas'] += 1 end
      end
    end
    
    if @estatisticas['respostas'] > 0
      @estatisticas['aproveitamento'] = ((@estatisticas['certas'].to_f / @estatisticas['respostas'])*100).round(0)
    end
    
    respond_to do |format|
      format.js
      format.html { render "assuntos" }
    end
  end
  
  def assuntos
    @assuntos = Assunto.all
  end
  
  def topico
    @topico = Topico.find_by(nome: params[:topico])
    @assunto = Assunto.find_by(nome: params[:assunto])
    
    @testes = []
    @respostas = []
    
    if @topico.testes.size > 0
      @topico.testes.each do |t|
        teste = Apostila.find_by(id: t)
        @testes.push(teste)
        #teste.questoes.each do |q|
        #  questao = Questao.find_by(id: q)
        #  resposta = Resposta.new
        #  @testes.push(questao)
        #  @respostas.push(resposta)
        #end
      end
    end
    
    @a = [["A) ", "a"]]
    @b = [["B) ", "b"]]
    @c = [["C) ", "c"]]
    @d = [["D) ", "d"]]
    @e = [["E) ", "e"]]
    
    render "topicos/show"
  end
  
  def new
    
  end
end
