class TopicosController < ApplicationController
  before_action :set_topico, only: [:show, :edit, :update, :destroy]
  before_action :set_semanas, only: [:edit]
  before_action :admin_only
  
  def admin_only
    if !current_user.admin
      redirect_to "/"
    end
  end
  
  # GET /topicos
  # GET /topicos.json
  def index
    @topicos = Topico.all
  end

  # GET /topicos/1
  # GET /topicos/1.json
  def show
    @assunto = Assunto.find_by(id: @topico.assunto)
  end
  
  def importar
    def contentful
      @client ||= Contentful::Client.new(
        access_token: "196f7e214bc56b834401b191907a021fd2ce2ec377c0854e63078910843857dd",
        space: "h8r98awupeov",
        dynamic_entries: :auto,
        raise_errors: true
      )
    end
    
    topicos = contentful.entries(content_type: "topico")
    
    topicos.each do |t|
      logger.debug "##########"
      logger.debug t.assunto
      logger.debug "##########"
      filho = Topico.where(nome: t.id)
      
      if filho.size == 0 then
        top = Topico.create! nome: t.id, titulo: t.titulo, texto: t.conteudo
      
        assunto = Assunto.where(nome: t.assunto.nome)
        if assunto.size > 0 then
          assunto = assunto.first
          top.assunto = assunto
          top.save
        end
      end
    end
    
    redirect_to '/topicos/'
  end

  # GET /topicos/new
  def new
    @topico = Topico.new
    @assuntos = Assunto.all
    @apostilas = Apostila.all
  end

  # GET /topicos/1/edit
  def edit
    @assuntos = Assunto.all
    @apostilas = Apostila.all
  end

  # POST /topicos
  # POST /topicos.json
  def create
    @topico = Topico.new(topico_params)
    
    respond_to do |format|
      if @topico.save
        format.html { redirect_to "/topicos/new", notice: 'Topico was successfully created.' }
        format.json { render :show, status: :created, location: @topico }
      else
        format.html { render :new }
        format.json { render json: @topico.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topicos/1
  # PATCH/PUT /topicos/1.json
  def update
    respond_to do |format|
      if @topico.update(topico_params)
        format.html { redirect_to @topico, notice: 'Topico was successfully updated.' }
        format.json { render :show, status: :ok, location: @topico }
      else
        format.html { render :edit }
        format.json { render json: @topico.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topicos/1
  # DELETE /topicos/1.json
  def destroy
    @topico.destroy
    respond_to do |format|
      format.html { redirect_to topicos_url, notice: 'Topico was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def adiciona_apostila
    @apostila = Apostila.find_by(id: params[:apostila])
    @topico = Topico.find_by(id: params[:topico])
    
    if not @topico.testes.include?(@apostila.id)
      @topico.testes.push(@apostila.id)
      @topico.save
    end
    
    redirect_to edit_topico_path(@topico)
  end
  
  def remove_apostila
    @apostila = Apostila.find_by(id: params[:apostila])
    @topico = Topico.find_by(id: params[:topico])
    
    @topico.testes.delete(@apostila.id)
    @topico.save
    redirect_to edit_topico_path(@topico)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topico
      @topico = Topico.find(params[:id])
    end

    def set_semanas
      @semanas = Semana.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topico_params
      params.require(:topico).permit(:nome, :assunto, :titulo, :texto, :exemplo, :testes, :indice, :semana_id)
    end
    
end
