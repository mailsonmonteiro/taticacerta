class AssuntosController < ApplicationController
  before_action :set_assunto, only: [:show, :edit, :update, :destroy]
  before_action :define_pagina

  def assunto
    @assunto = Assunto.find_by(nome: params[:assunto])

    m = curso_atual.materias
    @materias = Materia.where(:id.in => m)

    respond_to do |format|
      format.js
      format.html { render "assuntos" }
    end
  end
  
  def assuntos
    m = curso_atual.materias
    @materias = Materia.where(:id.in => m)
    @assunto = Assunto.all
  end
  
  def importar
    def contentful
      @client ||= Contentful::Client.new(
        access_token: "196f7e214bc56b834401b191907a021fd2ce2ec377c0854e63078910843857dd",
        space: "h8r98awupeov",
        dynamic_entries: :auto,
        raise_errors: true
      )
    end
    
    assuntos = contentful.entries(content_type: "assunto")
    
    assuntos.each do |a|
      m = Materia.where(nome: a.materia_abrev).first
      filhos = Assunto.where(nome: a.nome)    
        
      if filhos.size == 0
          Assunto.create! materia: m, nome: a.nome, display: a.display 
      else
        filhos.each do |f|
          f.materia = m
          f.nome = a.nome
          f.display = a.display
        end
      end
    end
    
    redirect_to '/assuntos/'
  end

#Funções básicas de scaffold------------------

  # GET /assuntos
  # GET /assuntos.json
  def index
    @assuntos = Assunto.all
  end

  # GET /assuntos/1
  # GET /assuntos/1.json
  def show
  end

  # GET /assuntos/new
  def new
    @assunto = Assunto.new
    @materias = Materia.all
  end

  # GET /assuntos/1/edit
  def edit
  end

  # POST /assuntos
  # POST /assuntos.json
  def create
    @assunto = Assunto.new(assunto_params)

    respond_to do |format|
      if @assunto.save
        format.html { redirect_to @assunto, notice: 'Assunto was successfully created.' }
        format.json { render :show, status: :created, location: @assunto }
      else
        format.html { render :new }
        format.json { render json: @assunto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assuntos/1
  # PATCH/PUT /assuntos/1.json
  def update
    respond_to do |format|
      if @assunto.update(assunto_params)
        format.html { redirect_to @assunto, notice: 'Assunto was successfully updated.' }
        format.json { render :show, status: :ok, location: @assunto }
      else
        format.html { render :edit }
        format.json { render json: @assunto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assuntos/1
  # DELETE /assuntos/1.json
  def destroy
    @assunto.destroy
    respond_to do |format|
      format.html { redirect_to assuntos_url, notice: 'Assunto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assunto
      @assunto = Assunto.find(params[:id])
      @materias = Materia.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assunto_params
      params[:assunto]
      params.require(:assunto).permit(:nome, :display, :descricao, :materia_id, :indice)
    end
    
    def define_pagina
      @pagina_atual = "assuntos"
    end
end
