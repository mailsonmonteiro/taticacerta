class ApostilasController < ApplicationController 
  before_action :set_apostila, only: [:show, :edit, :update, :destroy]
  before_action :set_semanas, only: [:edit]
  before_action :admin_only, except: [:apostila, :apostilas, :pagina, :corrige_cartao]
  
  def admin_only
    if !current_user.admin
      redirect_to "/"
    end
  end

  #########################
  #ADICIONA QUESTÃO NA APOSTILA
  #########################
  def adiciona_questao
      questao = Questao.find_by(id: params[:questao]) 
      @apostila = Apostila.find_by(id: params[:apostila])
      
      if not @apostila.questoes.include?(questao.id)
        @apostila.questoes.push(questao.id)
        @apostila.save
        @msg = "Questão adicionada com sucesso"
      else
        @msg = "A apostila já contem essa questão"
      end
  end
  
  #########################
  #REMOVE A QUESTÃO DA APOSTILA
  #########################
  def remove_questao
      questao = Questao.find_by(id: params[:questao]) 
      @apostila = Apostila.find_by(id: params[:apostila])
      
      @apostila.questoes.delete(questao.id)
      @apostila.save
  end
  
  #########################
  #Lista de provas e simulados
  #########################
  def apostilas
    @pagina_atual = "provas_e_simulados"
    @apostilas = Apostila.do_curso(curso_atual)
  end
  
  #########################
  #PROVAS E SIMULADOS -> APOSTILA
  #get provas_e_simulados/:apostila/[:indice]
  #########################
  def apostila
    @pagina_atual = "provas_e_simulados"
    
    #Redireciona se não for encontrada apostila
    if !params[:apostila].present? || Apostila.where(nome: params[:apostila]).size == 0 then  redirect_to "/provas_e_simulados/" and return end
    @apostila = Apostila.find_by(nome: params[:apostila])
    
    #Preparaa o cartão resposta
    @cartao_resposta = define_cartao
    @respostas = define_respostas @cartao_resposta

    render layout:"no_sidebar"
  end
  
  #########################
  #PROVAS E SIMULADOS -> APOSTILA
  #get provas_e_simulados/:apostila/[:indice]
  #########################
  def pagina
    @pagina = params[:indice]
    @indice = @pagina
    @apostila = Apostila.find_by(nome: params[:apostila])
    @cartao_resposta = define_cartao
    @anterior = "#{@apostila.link}/1"
    @proxima = "#{@apostila.link}/1"
    
    if @pagina.to_i > 0
      @questao = Questao.where(id: @apostila.questoes[@pagina.to_i-1]).first
      @resposta = @apostila.respostas.find_or_create_by(user_id: current_user.id, questao_id: @questao.id, cartao_id: @cartao_resposta.id)
      
      @indice = params[:indice].to_i
      @proxima = " #{@apostila.link}/#{@indice + 1}"
      if @indice > 1 then @anterior = "#{@apostila.link}/#{@indice-1}" end
      if @indice == @apostila.questoes.count then @proxima = "#{@apostila.link}/#{@indice}" end
      
      @pagina = "questoes/show"
    end
  end
  
  def corrige_cartao
    @apostila = Apostila.find_by(nome: params[:apostila])
    @cartao_resposta = define_cartao
    @respostas = define_respostas(@cartao_resposta)
    
    #defini se o sistema faz uma checagem numerica ou questao a questão.
    
    @cartao_resposta.aberto = false
  end

  # GET /apostilas
  # GET /apostilas.json
  def index
    @apostilas = Apostila.all
  end

  # GET /apostilas/1
  # GET /apostilas/1.json
  def show
    lista_questoes
  end

  # GET /apostilas/new
  def new
    @apostila = Apostila.new
    @materias = Materia.all
    @cursos = Curso.all
  end

  # GET /apostilas/1/edit
  def edit
  end

  # POST /apostilas
  # POST /apostilas.json
  def create
    @apostila = Apostila.new(apostila_params)
    
    respond_to do |format|
      if @apostila.save
        format.html { redirect_to @apostila, notice: 'Apostila was successfully created.' }
        format.json { render :show, status: :created, location: @apostila }
      else
        format.html { render :new }
        format.json { render json: @apostila.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /apostilas/1
  # PATCH/PUT /apostilas/1.json
  def update
    respond_to do |format|
      if @apostila.update(apostila_params)
        format.html { redirect_to @apostila, notice: 'Apostila was successfully updated.' }
        format.json { render :show, status: :ok, location: @apostila }
      else
        format.html { render :edit }
        format.json { render json: @apostila.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apostilas/1
  # DELETE /apostilas/1.json
  def destroy
    @apostila.destroy
    respond_to do |format|
      format.html { redirect_to apostilas_url, notice: 'Apostila was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_apostila
      @apostila = Apostila.find(params[:id])
      @materias = Materia.all
      @cursos = Curso.all
    end

    def set_semanas
      @semanas = Semana.all
    end

    def define_cartao
      cartao = Cartao.find_or_create_by(apostila_id: @apostila.id, user_id: current_user.id, aberto: true)
      cartao
    end
    
    def define_respostas(cartao_resposta)
      respostas = {}
      cartao_resposta.respostas.each do |r|
        respostas[r.questao_id] = r
      end
      respostas
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def apostila_params
      params.require(:apostila).permit(:nome, :descricao, :display, :curso, :texto, :semana_id, materias: [], assuntos: [])
    end
    
    def lista_questoes
      @questoes = []
      
      if @apostila != nil
        @apostila.questoes.each do |q|
          if q != ""
            questao = Questao.find_by(id: q)
            @questoes.push(questao)
          end
        end
      end
      
    end
    
end
