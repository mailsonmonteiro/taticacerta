class RespostasFsController < ApplicationController
  before_action :set_resposta_fs, only: [:show, :edit, :update, :destroy]

  # GET /respostas_fs
  # GET /respostas_fs.json
  def index
    @respostas_fs = RespostaFs.all
  end

  # GET /respostas_fs/1
  # GET /respostas_fs/1.json
  def show
  end

  # GET /respostas_fs/new
  def new
    @resposta_fs = RespostaFs.new
  end

  # GET /respostas_fs/1/edit
  def edit
  end

  # POST /respostas_fs
  # POST /respostas_fs.json
  def create
    @resposta_fs = RespostaFs.new(resposta_fs_params)

    respond_to do |format|
      if @resposta_fs.save
        format.html { redirect_to @resposta_fs, notice: 'Resposta fs was successfully created.' }
        format.json { render :show, status: :created, location: @resposta_fs }
      else
        format.html { render :new }
        format.json { render json: @resposta_fs.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /respostas_fs/1
  # PATCH/PUT /respostas_fs/1.json
  def update
    respond_to do |format|
      if @resposta_fs.update(resposta_fs_params)
        format.html { redirect_to @resposta_fs, notice: 'Resposta fs was successfully updated.' }
        format.json { render :show, status: :ok, location: @resposta_fs }
      else
        format.html { render :edit }
        format.json { render json: @resposta_fs.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /respostas_fs/1
  # DELETE /respostas_fs/1.json
  def destroy
    @resposta_fs.destroy
    respond_to do |format|
      format.html { redirect_to respostas_fs_url, notice: 'Resposta fs was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resposta_fs
      @resposta_fs = RespostaFs.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resposta_fs_params
      params[:resposta_fs]
    end
end
