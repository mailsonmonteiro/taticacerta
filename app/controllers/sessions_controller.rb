class SessionsController < ApplicationController
  def create
    begin
      @user = User.from_omniauth(request.env['omniauth.auth'])
      if @user.curso_atual == nil then @user.curso_atual = Curso.first.id.to_s end
      session[:user_uid] = @user.uid
      flash[:success] = "Seja bem vindo, #{@user.name}! Bons estudos."
    rescue Exception => e
      flash[:warning] = "There was an error while trying to authenticate you... #{e}"
    end
    redirect_to "/plano_de_estudo/"
  end
  
  def destroy
    if current_user
      session.delete(:user_uid)
      flash[:success] = 'See you!'
    end
    redirect_to root_path
  end
  
  def seleciona_curso
    novo_curso = params[:novo_curso]
    user = current_user
    curso = Curso.where(nome: novo_curso).first.id.to_s
    
    user.curso_atual = curso
    user.save
    
    redirect_to '/plano_de_estudo/'
  end

end