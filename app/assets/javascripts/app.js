/*
Substituir $(document).ready por:
$(document).on "turbolinks:load", ->
  
Substituir $(element).click por:
$(document).on "click", "[data-behavior~=update-credit-card]", =>
*/

$(document).on('turbolinks:load', function(){
  $('.hidden').hide();
  $('.hidden').removeClass('hidden');
  
  $('.ui.checkbox').checkbox();
  iniciaLandingPage();
});

function textoAssociado(){
  var texto = $('.texto-associado-txt')
  
  alert(texto);
}

function iniciaLandingPage(){
  $(".bloco").visibility({
    onBottomVisible:function(){
      $(this).transition("scale")
    }
  })
  
  $(".bloco.fadeup").visibility({
    onBottomVisible:function(){
      $(this).transition("fade up")
    }
  })
  
  $(".bloco.jiggle").visibility({
    onBottomVisible:function(){
      $(this).transition("jiggle")
    }
  })
}

function iniciaProgressBar(){
  $('.progress').progress();
}

function showSpinner(target){
  $('#'+target).html("<%= j render "layouts/spinner" %>");
}