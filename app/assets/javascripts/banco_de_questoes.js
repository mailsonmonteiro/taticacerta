//Banco de questões
function iniciaBdQ(){
  var condicao = $('#sidebar_listas').hasClass("active");
  
  if (condicao) {
      $('#filtros').hide();
      $('.esconde').hide();
      
      $('.dropdown')
        .dropdown({
          useLabels: false,
          message: {count: '{count} selecionado(s)'}
        })
      ;
  }
}

function esconde_filtros(){
  $('#filtros').toggle();
  $('#btn_filtros').toggleClass('basic');
}

function filtrar(){
  var materias = $('#dropdown_bloco').val();
  var assuntos = $('#dropdown_assunto').val();
  var atividade = $('input[name=throughput]:checked', '#form_atividade').val();
  
  filtros = {};
  
  //adiciona o filtros caso selecionados
  if (materias != null) {
    filtros.materias = materias;
  }
  
  if (assuntos != null) {
    filtros.assuntos = assuntos;
  }
  
  if (atividade != "") {
    filtros.atividade = atividade;
  };
  
  var c = jQuery.param(filtros);
  
  var url = document.location.href;
  var fim = window.location.href.indexOf('banco_de_questoes')+17;
  url = url.substring(0,fim);
  
  url = url+"?"+c;

  document.location = url;  
}

function exibeComentarios(q){
  var elemento = "#comentarios_"+q

  $(elemento).html('<i class="notched circle loading icon"></i>');
}

function exibeResolucao(q){
  var elemento = $(q)

  elemento.toggle();
}