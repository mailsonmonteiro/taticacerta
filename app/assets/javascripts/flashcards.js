(() => {
  const application = Stimulus.Application.start()

  application.register("slideshow", class extends Stimulus.Controller {
    static get targets() {
      return [ "slide", "respondido", "virar", "desempenho" ]
    }

    //Início
	  initialize() {
	  	MathJax.Hub.Typeset()
	  	//Define o número de slides
	  	this.size = this.slideTargets.length
			
			//Define o slide inicial e inicia a apresentação
			const index = parseInt(this.element.getAttribute("data-slideshow-index"))
			this.index = index
    	this.showSlide(index)

	    //Atualiza a barra de progresso
	    this.updateProgress()
	  }

	  //Próximo slide
	  next() {
    	if (this.index < this.size-1){
    		this.showSlide(this.index + 1)
    		this.updateProgress()
    		this.escondeOpcoes()
			}
  	}

  	//Slide anterior
	  previous() {
	  	if (this.index > 0){
		    this.showSlide(this.index - 1)
		    this.updateProgress()
		    this.escondeOpcoes()
	    }
	  }

	  //Exibe slide específico e força que todo os slides virem para a frente
	  showSlide(index) {
	    this.index = index
	    this.slideTargets.forEach((el, i) => {
	    	el.classList.remove('flip')
	      el.classList.toggle("slide--current", index == i)
	    })
	    //Atualiza o íd do flashcard atual
	    this.flashcard_id = document.querySelector('.slide--current').getAttribute('data-fid')
	    this.fixOverflow()

	  }

	  //Gira slide
	  flip() {
	  	//procura o slide atual e gira
	  	var ind = this.index
	    this.slideTargets.forEach((el, i) => {
	      if (ind == i) { el.classList.toggle('flip') } 
	    })
	    this.inverteOpcoes()
	  }

	  //Atualiza barra de progresso
	  updateProgress() {
	  	var progresso = ((this.index + 1) / this.size) * 100
	  	$('#deck_progress').progress({
    		percent: progresso
    	})
	  }

	  //Exibe e esconde menu de auto avaliação
	  inverteOpcoes(){
	  	this.respondidoTargets.forEach((el, i) => {
	    	el.classList.toggle('escondido')
	    })
	    this.virarTargets.forEach((el, i) => {
	    	el.classList.toggle('escondido')
	    })
	  }

	  escondeOpcoes(){
	  	this.respondidoTargets.forEach((el, i) => {
	    	el.classList.add('escondido')
	    })
	  	this.virarTargets.forEach((el, i) => {
	    	el.classList.remove('escondido')
	    })	    
	  }

	  //define o desempenho
	  desempenho(event){
	  	var desemp = event.target.getAttribute("data-desemp")
	  	document.getElementById("alt_"+this.flashcard_id).value = desemp
	  	$('[data-form='+this.flashcard_id+']').submit()
	  }

		desempenhoT(desemp){
			//verifica se o menu de desempenho está visível
			if (this.respondidoTarget.classList.contains('escondido') == false){
		  	document.getElementById("alt_"+this.flashcard_id).value = desemp
		  	$('[data-form='+this.flashcard_id+']').submit()
		  }
	  }

	  fixOverflow(){
	   	var slide = document.querySelector('.slide--current .flipper .front')
	    var bigger = true
	    while (checkOverflow(slide) && bigger){
	    	var newFs = parseInt(window.getComputedStyle(slide, null).fontSize)
	    	if (newFs > 12){
	    		slide.style.fontSize = (newFs - 1) + 'px'
	    	}else{
	    		bigger = false
	    	}
	    }
	    var slide = document.querySelector('.slide--current .flipper .back')
	   	var bigger = true
	    while (checkOverflow(slide) && bigger){
	    	var newFs = parseInt(window.getComputedStyle(slide, null).fontSize)
	    	if (newFs > 12){
	    		slide.style.fontSize = (newFs - 1) + 'px'
	    	}else{
	    		bigger = false
	    	}
	    }
	  }

  })

  //Hack necessário para capturar eventos do keyboard em toda a tela
  document.addEventListener("keydown", event => {
    element = document.querySelector(".ui.main.grid.container")
    slideshow = application.getControllerForElementAndIdentifier(element, "slideshow")
    //event.preventDefault();
    
    if (event.keyCode === 39) {
			slideshow.next()
    }else if (event.keyCode === 37){
    	slideshow.previous()
    }else if (event.keyCode === 32){
    	slideshow.flip()
    }else if (event.keyCode === 38){
    	slideshow.flip()
    }else if (event.keyCode === 40){
    	slideshow.flip()
    }else if (event.keyCode === 49){
    	slideshow.desempenhoT(1)
    }else if (event.keyCode === 50){
    	slideshow.desempenhoT(2)
    }else if (event.keyCode === 51){
    	slideshow.desempenhoT(3)
    }

  });

  // Determines if the passed element is overflowing its bounds,
	// either vertically or horizontally.
	// Will temporarily modify the "overflow" style to detect this
	// if necessary.
	function checkOverflow(el)
	{
	   var curOverflow = el.style.overflow;

	   if ( !curOverflow || curOverflow === "visible" )
	      el.style.overflow = "hidden";
	   		var isOverflowing = el.clientWidth < el.scrollWidth 
	     		|| el.clientHeight < el.scrollHeight;
	   		
	   		
	  		el.style.overflow = curOverflow;

	   return isOverflowing;
	}

}
)()