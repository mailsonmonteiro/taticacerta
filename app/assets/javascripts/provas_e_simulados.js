$(document).on('turbolinks:load', function(){
  cartao_resposta();
  paddingFix();
});

function ajax_link(){
  $('.dimmer.questao').dimmer('show');
}

function paddingFix(){
  $('#main_column').css('padding-left: 0')
}

function exibeMenu(){
  $('.ui.tiny.modal')
    .modal({
      centered: true
    })
    .modal('show')
  ;
}

function fechaMenu(){
  $('.ui.tiny.modal')
    .modal('hide')
  ;
}

function exibeGabarito() {
  $('.gabarito').removeClass('hidden');
}

function exibeResolucao(q){
  var elemento = $(q)

  elemento.toggleClass('hidden');
}

function cartao_resposta(){
  $('#cartao_resposta').show();
  $('#frame').html('');
  $('.dimmer.questao').dimmer('hide');
}