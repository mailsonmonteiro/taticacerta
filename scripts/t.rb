query = {
    token: 'B13D7B6159404F858C2AB12F36EB7E73',
    email: 'mailson.a.m.monteiro@gmail.com',
    currency: 'BRL',
    itemId1: '0001',
    itemDescription1: 'Produto PagSeguroI',
    itemAmount1: '99999.99',
    itemQuantity1: '1',
    itemWeight1: '1000',
    itemId2: '0002',
    itemDescription2: 'Produto PagSeguroII',
    itemAmount2: '99999.98',
    itemQuantity2: '2',
    itemWeight2: '750',
    reference: 'REF1234',
    senderName: 'Jose Comprador',
    senderAreaCode: '99',
    senderPhone: '99999999',
    senderEmail: 'comprador@uol.com.br',
    shippingType: '1',
    shippingAddressStreet: 'Av. PagSeguro',
    shippingAddressNumber: '9999',
    shippingAddressComplement: '99o andar',
    shippingAddressDistrict: 'Jardim Internet',
    shippingAddressPostalCode: '99999999',
    shippingAddressCity: 'Cidade Exemplo',
    shippingAddressState: 'SP',
    shippingAddressCountry: 'ATA'
}

header = 'Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1'

url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/'

response = HTTParty.post(url, header: header,query: query)

puts response