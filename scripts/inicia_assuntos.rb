m = Materia.matematica.first
Assunto.all.delete

# Conhecimentos Numéricos / numericos
Assunto.create! materia: m, display:"Operações em Conjuntos Numéricos", nome: "conjuntos"
Assunto.create! materia: m, display:"Desigualdade, Divisibilidade, Fatoração"
Assunto.create! materia: m, display:"Razões e Proporções, Porcentagem e Juros", nome: "razoes-proporcoes-porcentagem-e-juros"
Assunto.create! materia: m, display:"Relações de Dependência entre Grandezas"
Assunto.create! materia: m, display:"Sequências e Progressões, Princípios de Contagem"


# Conhecimentos Geometricos / geometricos
Assunto.create! materia: m, display:"Características das Figuras Geométricas Planas e Espaciais"
Assunto.create! materia: m, display:"Grandezas, Unidades de Medidas e Escalas"
Assunto.create! materia: m, display:"Comprimentos, Áreas e Volumes"
Assunto.create! materia: m, display:"Ângulos"
Assunto.create! materia: m, display:"Posições de Retas"
Assunto.create! materia: m, display:"Simetrias de Figuras Planas ou Espaciais"
Assunto.create! materia: m, display:"Congruência e Semelhança de Triângulos"
Assunto.create! materia: m, display:"Teorema de Tales"
Assunto.create! materia: m, display:"Relações Métricas nos Triângulos"
Assunto.create! materia: m, display:"Circunferências, Trigonometria do Ângulo Agudo"


# Conhecimentos de Estatística e Probabilidade / estat_e_prob
Assunto.create! materia: m, display:"Representação e Análise de Dados"
Assunto.create! materia: m, display:"Medidas de Tendência Central"
Assunto.create! materia: m, display:"Desvios e Variância"
Assunto.create! materia: m, display:"Noções de Probabilidade"

# Conhecimentos Algébricos / algebricos
Assunto.create! materia: m, display:"Gráficos e Funções"
Assunto.create! materia: m, display:"Funções Algébricas do 1º e do 2º Grau"
Assunto.create! materia: m, display:"Funções Polinomiais, Racionais, Exponenciais e Logarítmicas"
Assunto.create! materia: m, display:"Equações e Inequações"
Assunto.create! materia: m, display:"Relações no Ciclo Trigonométrico"
Assunto.create! materia: m, display:"Funções Trigonométricas"

# Conhecimentos Algébricos/Geométricos / alge_geo
Assunto.create! materia: m, display:"Plano Cartesiano"
Assunto.create! materia: m, display:"Retas"
Assunto.create! materia: m, display:"Circunferências"
Assunto.create! materia: m, display:"Pararelismo e Perpendicularidade"
Assunto.create! materia: m, display:"Sistema de Equações", nome:"sistemas-de-equacoes"

c = 0

Assunto.all.each do |a|
    c += 1
    puts c.to_s + a.display 
end
