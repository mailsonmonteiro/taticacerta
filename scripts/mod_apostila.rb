puts "################################################"
puts "CRIAÇÃO DE APOSTILA"
puts "################################################"
a = Apostila.find_by(nome: "trigonometria")

nome = "trigonometria"
materia = "matematica"
descricao = "TRIGONOMETRIA NO TRIÂNGULO RETÂNGULO"
display = "Trigonometria"

#a.materias.push(materia)
#a.nome = nome
#a.descricao = descricao
a.display = display

a.save

puts "Apostila salva com sucesso"
puts "Nome: " + a.nome.to_s
puts "Display name: " + a.display.to_s
puts "Matérias: " + a.materias.to_s
puts "Texto descritivo: " + a.descricao.to_s

puts "################################################"
puts "FIM DO SCRIPT"
puts "################################################"