m = Materia.matematica.first
Assunto.all.delete

# Conhecimentos Numéricos / numericos
Assunto.create! materia: m, display:"Matemática Básica", nome: "basica"

#Materias
fis = Materia.create! nome:"fisica", display:"Física", abrev:"Fis"
qui = Materia.create! nome:"quimica", display:"Química", abrev:"Qui"
bio = Materia.create! nome:"bioligia", display:"Bioligia", abrev:"Bio"
his = Materia.create! nome:"historia", display:"História", abrev:"His"
geo = Materia.create! nome:"geografia", display:"Geografia", abrev:"Geo"
por = Materia.create! nome:"portugues", display:"Português", abrev:"Por"
ing = Materia.create! nome:"ingles", display:"Inglês", abrev:"Ing"
soc = Materia.create! nome:"sociologia", display:"Sociologia", abrev:"Soc"
fil = Materia.create! nome:"filosofia", display:"Filosofía", abrev:"Fil"

#Semana 1
Assunto.create! materia: m, display:"Teoria elementar dos conjuntos", nome: "conjuntos"
Assunto.create! materia: m, display:"Conceitos básicos (trigonometria)", nome: "conceitos_basicos"
Assunto.create! materia: fis, display:"Introdução à cinemática", nome: "introducao_a_cinematica"
Assunto.create! materia: qui, display:"O Átomo", nome: "atomo"
Assunto.create! materia: bio, display:"Organização dos seres vivos e noções de biogenética", nome: "bioenergetica"
Assunto.create! materia: bio, display:"Evolução: conceitos e evidências [E]", nome: "evolucao"
Assunto.create! materia: his, display:"As origens da presença europeia no Brasil", nome: "origens_da_presenca_europeia_no_brasil"
Assunto.create! materia: geo, display:"Cartografia", nome: "cartografia"
Assunto.create! materia: por, display:"Morfologia – Classes gramaticais", nome: "morfologia_classes_gramaticais"
Assunto.create! materia: por, display:"Trovadorismo", nome: "trovadorismo"
Assunto.create! materia: ing, display:"Interpretação de textos: questões objetivas", nome: "questoes_objetivas"
Assunto.create! materia: soc, display:"Introdução à Sociologia", nome: "introducao_a_sociologia"
Assunto.create! materia: fil, display:"Um convite ao filosofar para a cidadania", nome: "um_convite_ao_filosofar"
